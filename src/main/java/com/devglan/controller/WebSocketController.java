package com.devglan.controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import com.devglan.RepeatTask;
import com.google.gson.Gson;

@Controller
@Service
public class WebSocketController {

	@Autowired
	private SimpMessageSendingOperations messagingTemplate;
	
	private Principal principal;
	
	//ArrayBlockingQueue<String> q = new ArrayBlockingQueue<String>(10);
	//RepeatTask rt = new RepeatTask(q);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	private void init(){
		
	}
	
	@MessageMapping("/message")
	@SendToUser("/queue/reply")
	public String processMessageFromClient(@Payload String message, Principal principal) throws Exception {
		String name = new Gson().fromJson(message, Map.class).get("name").toString();
		System.out.println("*****RECEIVED MSG now*****" + message);
		//messagingTemplate.convertAndSendToUser(principal.getName(), "/queue/reply", name);
		
		this.principal = principal;
		sendToClient();
		return name;
	}
	
	@MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }
	
	@Scheduled(initialDelay=1000,fixedRate = 10000)
    public void sendToClient() {
		if(principal!=null){
			String time = String.format("The time is now {}", dateFormat.format(new Date()));
    		messagingTemplate.convertAndSendToUser(principal.getName(),"/queue/fromserver", time);
    		System.out.println("*****Sending MSG now*****");
		}
    }
	
    
	

}
