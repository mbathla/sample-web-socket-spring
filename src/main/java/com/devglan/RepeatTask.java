package com.devglan;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Configuration
public class RepeatTask {
	
	private ArrayBlockingQueue<String> q = new ArrayBlockingQueue<String>(10);
	
	private void init(){
		
	}
	

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

   
    public void reportCurrentTime() {
    	System.out.println("reportCurrentTime");
        String time = String.format("The time is now {}", dateFormat.format(new Date()));
        q.add(time);
    }
}
